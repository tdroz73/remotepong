package examples

import akka.actor.ActorSystem
import examples.PingPongProtocol.Start

object Main extends App {
  val system = ActorSystem("PongSystem")
  val pingActor = system.actorOf(props = PongActor.props(maxPing = 100), name = "pongActor")
  pingActor ! Start
}
