package examples

import akka.actor.{Actor, Props}
import com.typesafe.scalalogging.LazyLogging
import examples.PingPongProtocol.{Ping, Pong, Start}

object PingPongProtocol {
  case object Start
  case class Ping(who : String)
  case class Pong(who : String, count : Int)
}

object PongActor {
  def props(maxPing : Int) = Props(new PongActor(maxPing))
}

class PongActor(maxPing : Int) extends Actor with LazyLogging {
  var pingCount = 0

  override def preStart(): Unit = logger.info(s"${self.path} - I'm alive!")

  override def postStop(): Unit = logger.info(s"${self.path} - Darn, I only processed $pingCount pings!")

  override def receive: Receive = {
    case Start =>
      logger.info("Staring pong actor...")

    case Ping(who) =>
      logger.info(s"Ping for $who...")
      pingCount = pingCount + 1
      sender ! Pong(who, pingCount)

    case unexpected => logger.info(s"I wasn't expecting this: $unexpected")
  }

}